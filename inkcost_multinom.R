source("load_mnist_data.R")
library(nnet)

# Ink feature:sum of all pixeldata in a row(/datapoint)
mnist.data$inkcost <- rowSums(mnist.data[,2:ncol(mnist.data)])
tapply(mnist.data$inkcost,mnist.data[,1],mean)
tapply(mnist.data$inkcost,mnist.data[,1],sd)

mnist.data$inkcost <- scale(mnist.data$inkcost)

mnist.multinom <- multinom(label ~ inkcost, data=mnist.data, maxit=1000)

mnist.multinom.pred <- predict(mnist.multinom, mnist.data[,-c(1)], type = "class")

confusionmatrix <- table(mnist.data[,1], mnist.multinom.pred)
print(confusionmatrix)
sum(diag(confusionmatrix))/sum(confusionmatrix)