library(ggplot2)

mnist.data <- read.csv("dataset/mnist.csv", header= TRUE)

mnist.data["label"] <- as.factor(mnist.data$label)

# Load library needed for describe
library(Hmisc)

# describe.output <- Hmisc::describe(mnist.data)

# Histogram with fixed amount of X-axis, 

# mnist_data_colnames <- colnames(mnist.data)
# for(mnist_pixel_column in 2:ncol(mnist.data)) {
#   feature_name <- mnist_data_colnames[mnist_pixel_column]
#   cat("Working on feature column:", feature_name, "\n")
# 
#   name <- paste("histogram-", feature_name, ".png", collapse = '', sep='')
#   pixel_column <- mnist.data[,mnist_pixel_column]
# 
#   ggplot(data=mnist.data, aes(mnist.data[,mnist_pixel_column])) +
#     geom_histogram(breaks=seq(0, 255, by=10)) + ylim(c(0,42000)) + labs(title=paste("Histogram for ", feature_name, sep=""), x="Ink (Light 0 - Dark 255)", y="Count")
# 
#   ggsave(name, width = 16, height = 9, dpi = 100)
# }

# Ink feature:sum of all pixeldata in a row(/datapoint)
mnist.data$inkcost <- rowSums(mnist.data[,2:ncol(mnist.data)])
tapply(mnist.data$inkcost,mnist.data[,1],mean)
tapply(mnist.data$inkcost,mnist.data[,1],sd)


symmetryfunc <- function(x) {
  dim(x)
  # Create a matrix from a single row
  eightexa <- matrix(unlist(x[2:785]),nrow = 28,ncol = 28, byrow = TRUE)
  
  # Create tophalf of the matrix
  matrixtop <- eightexa[1:14,]
  matrixbottom <- eightexa[15:28,]
  
  # Reverse matrixbottom
  matrixbottom <- matrixbottom[nrow(matrixbottom):1,]
  
  # Print to table file to view in nicer editor
  write.table(matrixtop, file = "matrixtop.txt", row.names=F, col.names=F, sep="\t")
  write.table(matrixbottom, file = "matrixbottom.txt", row.names=F, col.names=F, sep="\t")
  
  # Calculate minimum matrix
  matrixmin <- pmin(matrixtop, matrixbottom)
  write.table(matrixmin, file = "matrixmin.txt", row.names=F, col.names=F, sep="\t")
  
  matrixsymmetry <- sum(matrixmin)
  
  return(matrixsymmetry)
}


mnist.data$horzsymm <- apply(mnist.data[,2:ncol(mnist.data)], 1, symmetryfunc)

# Verify everything is a number
for(row in 1:nrow(mnist.data)){
  for(col in 1:ncol(mnist.data)){
    d <- mnist.data[row,col]
    print(d)
    stopifnot(all.equal(d, as.integer(d)))
  }
}


# filter all eights
eights <- mnist.data[mnist.data$label %in% 8,]

# Create a matrix from a single row
eightexa <- matrix(unlist(eights[1,2:785]),nrow = 28,ncol = 28, byrow = TRUE)

# Create tophalf of the matrix
matrixtop <- eightexa[1:14,]
matrixbottom <- eightexa[15:28,]

# Reverse matrixbottom
matrixbottom <- matrixbottom[nrow(matrixbottom):1,]

# Print to table file to view in nicer editor
write.table(matrixtop, file = "matrixtop.txt", row.names=F, col.names=F, sep="\t")
write.table(matrixbottom, file = "matrixbottom.txt", row.names=F, col.names=F, sep="\t")

# Calculate minimum matrix
matrixmin <- pmin(matrixtop, matrixbottom)
write.table(matrixmin, file = "matrixmin.txt", row.names=F, col.names=F, sep="\t")

matrixsymmetry <- sum(matrixmin)

# # Split into training and test set
# smp_size <- floor(0.75 * nrow(mnist.data))
# 
# ## set the seed to make your partition reproducible
# set.seed(123)
# train_ind <- sample(seq_len(nrow(mnist.data)), size = smp_size)
# 
# mnist.train <- mnist.data[train_ind, ]
# mnist.test <- mnist.data[-train_ind, ]

library(nnet)

mnist.data$horzsymm <- scale(mnist.data$horzsymm)

mnist.multinom <- multinom(label ~ horzsymm, data=mnist.data, maxit=1000)

mnist.multinom.pred <- predict(mnist.multinom, mnist.data[,-c(1)], type = "class")

confusionmatrix <- table(mnist.data[,1], mnist.multinom.pred)
print(confusionmatrix)
sum(diag(confusionmatrix))/sum(confusionmatrix)


library(nnet)

mnist.data$inkcost <- scale(mnist.data$inkcost)

mnist.multinom <- multinom(label ~ inkcost + horzsymm, data=mnist.data, maxit=1000)

mnist.multinom.pred <- predict(mnist.multinom, mnist.data[,-c(1)], type = "class")

confusionmatrix <- table(mnist.data[,1], mnist.multinom.pred)
print(confusionmatrix)
sum(diag(confusionmatrix))/sum(confusionmatrix)

# dsF <- "describe_output_full.txt"
# if (file.exists(dsF)) file.remove(dsF)
# capture.output(describe.output, file=dsF, append=TRUE)
# 
# output <- NULL;
# # v <- list()
# for (value in describe.output) {
#   labelV <- value$descript
#   distinctV <- value$counts["distinct"]
#   cv <- c(labelV, distinctV)
#   output <- cbind(output, cv)
# }
# 




