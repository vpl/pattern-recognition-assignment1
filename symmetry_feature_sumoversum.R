source("load_mnist_data.R")

symmetryfunc <- function(x) {
  # Create a matrix from a single row
  eightexa <- matrix(unlist(x[1:784]),nrow = 28,ncol = 28, byrow = TRUE)
  
  # Create tophalf of the matrix
  matrixtop <- eightexa[1:14,]
  print(matrixtop)
  matrixbottom <- eightexa[15:28,]
  print(matrixbottom)
  
  matrixsymmetry <- sum(matrixtop) / sum(matrixtop + matrixbottom)
  
  return(matrixsymmetry)
}

mnist.data$horzsymm <- apply(mnist.data[,2:ncol(mnist.data)], 1, symmetryfunc)

library(nnet)

mnist.data$horzsymm <- scale(mnist.data$horzsymm)

mnist.multinom <- multinom(label ~ horzsymm, data=mnist.data, maxit=1000)

mnist.multinom.pred <- predict(mnist.multinom, mnist.data[,-c(1)], type = "class")

confusionmatrix <- table(mnist.data[,1], mnist.multinom.pred)
accuracy <- sum(diag(confusionmatrix))/sum(confusionmatrix)

print(confusionmatrix)
print(accuracy)

mnist.data$inkcost <- rowSums(mnist.data[,2:ncol(mnist.data)])
mnist.data$inkcost <- scale(mnist.data$inkcost)

mnist.multinom.twofeatures <- multinom(label ~ horzsymm + inkcost, data=mnist.data, maxit=1000)
mnist.multinom.twofeatures.pred <- predict(mnist.multinom.twofeatures, mnist.data[,-c(1)], type = "class")

confusionmatrix.twofeatures <- table(mnist.data[,1], mnist.multinom.twofeatures.pred)
accuracy.twofeatures <- sum(diag(confusionmatrix.twofeatures)) / sum(confusionmatrix.twofeatures)

print(confusionmatrix.twofeatures)
print(accuracy.twofeatures)