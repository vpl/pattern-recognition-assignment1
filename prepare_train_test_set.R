# Create training and test data csv's

mnist.data <- read.csv("dataset/mnist.csv", header = TRUE)
mnist.data["label"] <- as.factor(mnist.data$label)

smp_size <- floor(0.75 * nrow(mnist.data))

set.seed(123)
train_ind <- sample(seq_len(nrow(mnist.data)), size = smp_size)

mnist.train <- mnist.data[train_ind, ]
mnist.test <- mnist.data[-train_ind, ]

write.csv(mnist.train, file = "mnist_train.csv", row.names = FALSE)
write.csv(mnist.test, file = "mnist_test.csv", row.names = FALSE)