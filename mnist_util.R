print_image <- function(x, res = 28) {
  # Put the pixel vector (pixel0, pixel1, etc... into a matrix by row)
  m <- matrix(x, nrow = res, ncol = res, byrow = TRUE)

  # Convert it to numeric for the image()
  m <- apply(m, 2, as.numeric)
  
  # Matrix m is in correct order but image() is drawn differently...
  m <- apply(m, 2, rev)
  m <- t(m)

  image(1:res, 1:res, m, col=gray((0:255)/255))
}

print_image_dataset <- function(x, label = 8, count = 1, res = 28) {
  col <- (res * res) + 1
  print_image(x[x$label == label, ][count,2:col], res = res)
}

downsize_resolution <- function(x, res = 28) {
  # Get all the names from the data
  names <- colnames(x)
  
  # Remove label
  names <- names[2:length(names)]
  
  # Give us a matrix representation
  matrix_representation <- matrix(names, nrow = res, ncol = res, byrow = TRUE)
  
  # List the rows we would like to remove
  delete_rows <- seq(1, nrow(matrix_representation), 2)
  
  # Delete it from the matrix
  matrix_representation <- matrix_representation[-delete_rows,]
  
  # List columns we would like to remove
  delete_cols <- seq(1, ncol(matrix_representation), 2)
  matrix_representation <- matrix_representation[,-delete_cols]
  
  # Back to a vector with the features we would like to keep
  v <- as.vector(t(matrix_representation))
  
  x[, c("label", v)]
}

downsize_resolution_sum <- function(x, res = 28) {
  # Skipping label in 1
  data <- x[, 2:ncol(x)]
  
  # Get all the names from the data
  names <- colnames(data)
  
  # Give us a matrix representation
  matrix_representation <- matrix(names, nrow = res, ncol = res, byrow = TRUE)

  # List the uneven rows
  delete_rows <- seq(1, nrow(matrix_representation), 2)
  
  # Create two matrices with each half of the rows (all even or all uneven rows)
  matrix_reven <- matrix_representation[-delete_rows,]
  matrix_rodd <- matrix_representation[delete_rows,]
  
  reven_features <- as.vector(t(matrix_reven))
  rodd_features <- as.vector(t(matrix_rodd))
  
  rcombined <- data[, reven_features] + data[, rodd_features]
  
  matrix_representation <- matrix(colnames(rcombined), nrow = (res / 2), ncol = res, byrow = TRUE)
  
  # List all uneven columns
  delete_cols <- seq(1, ncol(matrix_representation), 2)
  
  # Add the even columns too the even rows matrix
  matrix_ceven <- matrix_representation[,-delete_cols]
  # same for the uneven columns
  matrix_codd <- matrix_representation[,delete_cols]
  
  ceven_features <- as.vector(t(matrix_ceven))
  codd_features <- as.vector(t(matrix_codd))
  
  ccombined <- rcombined[, ceven_features] + rcombined[, codd_features]
  
  label <- x$label
  ccombined <- ccombined / 4

  combined <- cbind(label = label, ccombined)
  return(combined)
}

symmetryfunc <- function(x, res = 28) {
  # Create a matrix from a single row
  eightexa <- matrix(unlist(x),nrow = res,ncol = res, byrow = TRUE)
  
  half <- res / 2
  # Create tophalf of the matrix
  matrixtop <- eightexa[1:half,]

  matrixbottom <- eightexa[(half+1):res,]
  matrixsymmetry <- sum(matrixtop) / sum(matrixtop + matrixbottom)
  
  return(matrixsymmetry)
}

symmetryfunc14 <- function(x) {
  c <- symmetryfunc(x, res=14)
  return(c)
}

symmetryfunc28 <- function(x) {
  c <- symmetryfunc(x, res=28)
  return(c)
}

add_horizontal_symmetry <- function(x, end.col = 785) {
  x$horzsymm <- apply(x[,2:end.col], 1, symmetryfunc)

  return(x)
}


try_multinom <-function(x, formula) {
  library(nnet)

  multinom <- multinom(formula, data=x, maxit=1000)
  
  pred <- predict(multinom, x[,-c(1)], type = "class")
  
  confusionmatrix <- table(x[,1], pred)
  accuracy <- sum(diag(confusionmatrix))/sum(confusionmatrix)
  
  print(confusionmatrix)
  print(accuracy)
}

try_knn <- function(train, label.col = 1, k = 1) {
  library(class)
  classifiers <- train[,label.col]

  train_set <- train[, -c(label.col)]
  
  classification = knn.cv(train_set, classifiers, k)
  sum(classification == classifiers)/nrow(train_set)

}

try_knn_test <- function(train, test, label.col = 1, k = 1) {
  train_labels <- train[,1]
  test_labels <- test[,1]
  
  pred <- knn(train = train[,2:ncol(train)], test[,2:ncol(test)], cl = train_labels, k = k)
  
  dftestlabels <- data.frame(test_labels)
  
  # merge <- data.frame(pred, test_labels)
  # 
  # names(merge) <- c("Predicted label", "Actual label")
  
  CrossTable(x = test_labels, y = pred, prop.chisq=FALSE)
  
}

try_svm <- function(train, test, label.col = 1) {
  library(e1071)
  train_svm <- svm(train[, -c(label.col)], train[, c(label.col)])

  pred <- predict(train_svm, test[,-c(label.col)])

  ctable <- table(test[,label.col], pred)



  accuracy <- sum(diag(ctable))/nrow(test)

  print(ctable)
  print(accuracy)
  
  return(pred)
  }

tune_svm <- function(train, label.col = 1, cost = 1:10) {
  tune <- tune.svm(train[, -c(label.col)], train[, label.col], cost= cost)

  tune$performances
}

# Create training and test data csv's

read_mnist_data <- function() {
  mnist.data <- read.csv("dataset/mnist.csv", header = TRUE)
  mnist.data["label"] <- as.factor(mnist.data$label)
  
  return (mnist.data)
}

write_test_train_csv <- function(train, test) {
  write.csv(train, file = "mnist_train.csv", row.names = FALSE)
  write.csv(test, file = "mnist_test.csv", row.names = FALSE)
}

create_test_train_percentage <- function(percentage = 0.75) {
  mnist.data <- read_mnist_data()
  
  smp_size <- floor(percentage * nrow(mnist.data))
  
  set.seed(123)
  train_ind <- sample(seq_len(nrow(mnist.data)), size = smp_size)
  
  mnist.train <- mnist.data[train_ind, ]
  mnist.test <- mnist.data[-train_ind, ]
  
  write_test_train_csv(mnist.train, mnist.test)
}

create_test_train_number <- function(count = 5000) {
  mnist.data <- read_mnist_data()
  
  set.seed(123)
  train_ind <- sample(seq_len(nrow(mnist.data)), size = count)
  
  mnist.train <- mnist.data[train_ind, ]
  mnist.test <- mnist.data[-train_ind, ]
  
  write_test_train_csv(mnist.train, mnist.test)
}

load_train_data <- function(version = "") {
  file <- paste("dataset/mnist_train", version, ".csv", sep="")
  
  mnist_train <- read.csv(file, header = TRUE)
  mnist_train["label"] <- as.factor(mnist_train$label)
  
  return(mnist_train)
}

load_test_data <- function(version = "") {
  file <- paste("dataset/mnist_test", version, ".csv", sep="")
  
  mnist_test <- read.csv(file, header = TRUE)
  mnist_test["label"] <- as.factor(mnist_test$label)
  
  return(mnist_test)
}

to_classifiers <- function(x) {
  x[, 1]
}

to_cases <- function(x) {
  x[, 2:ncol(x)]
}

remove_near_zero_var <- function(mnist) {
  library(caret)
  nzv_cols <- nearZeroVar(mnist[,2:ncol(mnist)])
  mnist[, -nzv_cols]
}

show_image_nzv <- function(nzv)  {
  chosen <- 1:784 %in% nzv
  
  # True (black): removed
  # False (white): kept
  #chosen <- logical(length = 784)
  
  matrix <- matrix(chosen, 28, 28, byrow = TRUE)
  print(matrix)
  
  matrix <- apply(matrix, 2, rev)
  
  image(1:28, 1:28, t(matrix), col=grey(1:0))
}

prep_dataset <- function(train, test) {
  library(caret)
  nzv <- nearZeroVar(train)
  
  mnist_train_nzv <- mnist_train[, -nzv]
  mnist_test_nzv <- mnist_test[, -nzv]
  
  train_labels <- mnist_train_nzv$label
  test_labels <- mnist_test_nzv$label
  
  mnist_train_nzv_div <- mnist_train_nzv[, 2:ncol(mnist_train_nzv)] / 255
  mnist_test_nzv_div <- mnist_test_nzv[, 2:ncol(mnist_test_nzv)] / 255
  
  mnist_train_nzv_div <- cbind(train_labels, mnist_train_nzv_div)
  mnist_test_nzv_div <- cbind(test_labels, mnist_test_nzv_div)
  
  list(mnist_train_nzv_div, mnist_test_nzv_div)
}
